import { ref, onMounted, watch } from "vue";
import { Camera, CameraResultType, CameraSource, Photo } from "@capacitor/camera";
import { Filesystem, Directory } from "@capacitor/filesystem";
import { Storage } from "@capacitor/storage";

const fotos = ref<FotoUsuario[]>([]);

export function usarGaleriaFotos() {
    const tirarFoto = async () => { 

        // Chama a camera para tirar a foto
        const cameraFoto = await Camera.getPhoto({
            resultType: CameraResultType.Uri,
            source: CameraSource.Camera,
            quality: 100
        });

        // Guarda a foto tirada dentro da nossa variavel fotos
        const nomeArquivo = new Date().getTime() + ".jpeg"; // 1113.jpeg

        const arquivoImagemSalvo = {
            filepath: nomeArquivo,
            webviewPath: cameraFoto.webPath
        };

        fotos.value = [arquivoImagemSalvo, ...fotos.value]

    };

    return{
        fotos,
        tirarFoto
    };
}

export interface FotoUsuario {
    filepath: string;
    webviewPath?: string;
}